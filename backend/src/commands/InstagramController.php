<?php

namespace app\commands;

use Imagine\Gd\Imagine;
use yii\console\Controller;
use Phpfastcache\Helper\Psr16Adapter;
use \yii\imagine\Image;

class InstagramController extends Controller
{

    public function actionTest()
    {
        $account = \app\models\Accounts::findOne(['username' => 'vodyanovi']);
        $account->lastPost = [
            'test' => 'huest',
            'one' => 'two',
            'tree' => 'four',
        ];
        $account->dateUpdate = time();
        var_dump($account->lastPost);
        var_dump($account->save());
        var_dump($account->lastPost);
    }

    public function actionIndex()
    {
        $account = [
            'username' => \Yii::$app->params['instagram']['username'],
            'password' => \Yii::$app->params['instagram']['password'],
        ];
        $size = new \Imagine\Image\Box(\Yii::$app->params['image']['width'], \Yii::$app->params['image']['height']);
        $mode = \Imagine\Image\ImageInterface::THUMBNAIL_OUTBOUND;
        $imagePath = \Yii::getAlias(\Yii::$app->params['image']['path']);
        $imageWebPath = \Yii::getAlias(\Yii::$app->params['image']['web']);
        $imagine = new \Imagine\Gd\Imagine();

        $this->trace('Запуск получения новых постов аккаунтов инстаграмма');
        /** @var \app\models\Accounts[] */
        $accounts = \app\models\Accounts::find()->all();
        $instagram = \InstagramScraper\Instagram::withCredentials(
            new \GuzzleHttp\Client(),
            $account['username'],
            $account['password'],
            new Psr16Adapter('Files')
        );
        $instagram->login(); // will use cached session if you want to force login $instagram->login(true)
        $instagram->saveSession();  //DO NOT forget this in order to save the session, otherwise have no sense
        foreach ($accounts as $i => $account) {
            if ($account->dateUpdate > strtotime('-9 minute')) {
                continue;
            }
            $this->trace([
                'Получение новых постов аккаунта ',
                $account->username,
            ]);
            $medias = $instagram->getMedias($account->username);
            if (!empty($medias)) {
                $this->trace('Сохранение поста аккаунта ' . $account->username);
                $image = $medias[0]->getImageHighResolutionUrl();
                $imageName = $account->id . '.jpg';
                $accountImagePath = $imagePath . '/' .  $imageName;
                $accountImageWeb = $imageWebPath . '/' . $imageName;
                $imagine->open($image)->thumbnail($size, $mode)->save($accountImagePath);
                list($width, $height) = getimagesize($image);
                $account->lastPost = [
                    'image' => $accountImageWeb,
                    'originalImage' => $image,
                    'commentsCount' => $medias[0]->getCommentsCount(),
                    'likesCount' => $medias[0]->getLikesCount(),
                    'url' => $medias[0]->getLink(),
                    'isSquare' => $width == $height,
                    'isHorizontal' => $width > $height,
                    'isVertical' => $width < $height,
                ];
                $account->dateUpdate = time();
                $account->save();
            }
            $sleep = rand(10, 30);
            $this->trace('Засыпаем на ' . $sleep);
            sleep($sleep);
        }
    }

    private function trace($message)
    {
        echo date("d.m.Y H:i:s - ") . print_r($message, 1) . " \r\n";
    }
}
