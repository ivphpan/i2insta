<?php

/** 
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \app\models\Accounts $model
 */

$this->title = 'Популярные страницы инстаграма';
?>
<div class="photo-wrapper">
    <?php foreach ($instagramLastPost as $lastPost) : ?>
        <div class="photo" style="background:url(<?= $lastPost['image'] ?>)">
            <div>
                <a href="https://instagram.com/<?= $lastPost['username'] ?>/" target="_blank" class="photo-user-url">@<?= $lastPost['username'] ?></a>
            </div>
            <a href="<?= $lastPost['url'] ?>" target="_blank" class="photo-url"></a>
            <div class="photo-info">
                <div class="comments-count">
                    💬 <?= $lastPost['commentsCount'] ?>
                </div>
                <div class="photo-space">

                </div>
                <div class="likes-count">
                    ❤ <?= $lastPost['likesCount'] ?>
                </div>
            </div>

        </div>
    <?php endforeach ?>
</div>


<style>
    .photo-wrapper {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-around;
    }


    .photo {
        width: 300px;
        height: 330px;
        display: flex;
        justify-content: space-between;
        flex-direction: column;
        margin-bottom: 20px;
    }

    .photo-info {
        display: flex;
        justify-content: space-between;
        width: 100%;
    }

    .photo-user-url {
        padding: 10px 20px;
        background: rgba(000, 000, 000, 0.4);
        border-radius: 0 0 10px 0;
        display: inline-block;
        visibility: hidden;
        color: #f2f2f2;
        font-weight: bold;
    }

    .photo-user-url:hover {
        text-decoration: none;
        color: #f2f2f2;
    }

    .photo:hover .photo-user-url {
        visibility: visible;
    }

    .photo-url {
        height: 100%;
        display: block;
    }

    .photo-url:hover {
        text-decoration: none;
    }

    .comments-count {
        padding: 10px 20px;
        background-color: rgba(000, 000, 999, 0.3);
        border-radius: 0 10px 0 0;
        font-size: 14px;
        color: #f2f2f2;
    }

    .likes-count {
        padding: 10px 20px;
        background-color: rgba(999, 000, 000, 0.3);
        border-radius: 10px 0 0 0;
        font-size: 14px;
        color: #f2f2f2;
    }

    .photo-space {
        flex-grow: 1;
    }
</style>