<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'instagram' => [
        'username' => 'cat.chivas',
        'password' => '2021chivasCat',
    ],
    'image' => [
        'width' => 300,
        'height' => 330,
        'path' => '@webroot/images/accounts',
        'web' => 'images/accounts',
    ]
];
