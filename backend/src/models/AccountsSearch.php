<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Accounts;

/**
 * AccountsSearch represents the model behind the search form of `app\models\Accounts`.
 */
class AccountsSearch extends Accounts
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'dateAdd', 'dateUpdate'], 'integer'],
            [['username', 'lastPost'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Accounts::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dateAdd' => $this->dateAdd,
            'dateUpdate' => $this->dateUpdate,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'lastPost', $this->lastPost]);

        return $dataProvider;
    }

    public function getInstagramLastPost()
    {
        $params = ['limit' => 10];
        $dataProvider = $this->search($params);
        $items = [];

        foreach ($dataProvider->models as $model) {
            if (empty($model->lastPost)) {
                continue;
            }
            $items[$model->id] = $model->lastPost;
            $items[$model->id]['image'] = \Yii::$app->request->baseUrl . '/' . $items[$model->id]['image'];
            $items[$model->id]['username'] = $model->username;
        }
        shuffle($items);
        return $items;
    }
}
