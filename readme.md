# i2instagram - Модуль последних постов Instagram 

Для установки и использования понадобавится **docker** **docker-compose**

Запускаем сборку с выводом лога:

`docker-compose up --build`

Запускаем еще одно окно терминала и запускаем команду установки yii2:

`docker-compose exec backend composer install `

Затем запускаем команду создания таблицы аккаунтов:

`docker-compose exec backend ./yii migrate`
### Для получения постов с Instagram

Необходимо изменить настройки по адресу:

`backend/src/config/params.php`

Изменить данные:

`username` - логин от инстаграм

`password` - пароль от интаграм

### `~ Всё готово ! ~`

Админка аккаунтов доступна по адресу: [http://localhost:8080/accounts](http://localhost:8080/accounts)

Страницы с последними постами будет доступна по адресу: [http://localhost:8080/](http://localhost:8080/)

