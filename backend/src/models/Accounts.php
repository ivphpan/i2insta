<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%accounts}}".
 *
 * @property int $id
 * @property string|null $username Username
 * @property int|null $dateAdd Время добавления
 * @property int|null $dateUpdate Дата последнего обновления
 * @property string|null $lastPost Последний пост
 */
class Accounts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%accounts}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dateAdd', 'dateUpdate'], 'integer'],
            ['lastPost', 'safe'],
            [['username'], 'string', 'max' => 255],
            ['username', 'required'],
            ['username', 'unique', 'on' => ['create'], 'message' => 'Аккаунт с таким логином уже добавлен'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Инстаграм логин'),
            'dateAdd' => Yii::t('app', 'Время добавления'),
            'dateUpdate' => Yii::t('app', 'Дата последнего обновления'),
            'lastPost' => Yii::t('app', 'Последний пост'),
        ];
    }

    public function beforeSave($insert)
    {
        $this->lastPost = \yii\helpers\Json::encode($this->lastPost);
        return true;
    }

    public function afterValidate()
    {
        if ($this->scenario == 'create') {
            $this->dateAdd = time();
        }
        return true;
    }

    public function afterFind()
    {
        if (!empty($this->lastPost)) {
            $this->lastPost = \yii\helpers\Json::decode($this->lastPost);
        }
    }
}
