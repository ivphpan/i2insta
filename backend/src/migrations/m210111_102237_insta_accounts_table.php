<?php

use yii\db\Migration;

/**
 * Class m210111_102237_insta_accounts_table
 */
class m210111_102237_insta_accounts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{accounts}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->comment('Username'),
            'dateAdd' => $this->integer()->comment('Время добавления'),
            'dateUpdate' => $this->integer()->comment('Дата последнего обновления'),
            'lastPost' => $this->json()->comment('Последний пост'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{accounts}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210111_102237_insta_accounts_table cannot be reverted.\n";

        return false;
    }
    */
}
